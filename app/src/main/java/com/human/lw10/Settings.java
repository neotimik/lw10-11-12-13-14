package com.human.lw10;


import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class Settings extends PreferenceActivity implements Preference.OnPreferenceChangeListener
{
    ListPreference collection;
    ListPreference color;
    CharSequence[] pictValue, colValue;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        collection = (ListPreference)this.findPreference("PictureCollection");
        color = (ListPreference)this.findPreference("BackgroundColor");

        collection.setOnPreferenceChangeListener(this);
        color.setOnPreferenceChangeListener(this);

        collection.setSummary(collection.getEntry());
        color.setSummary(color.getEntry());

        pictValue = collection.getEntries();
        colValue = color.getEntries();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue)
    {
        String Key = preference.getKey();
        if (Key.equals("PictureCollection"))
        {
            int i = ((ListPreference)preference).findIndexOfValue(newValue.toString());
            preference.setSummary(pictValue[i]);
            return true;
        }

        if (Key.equals("BackgroundColor"))
        {
            int i = ((ListPreference)preference).findIndexOfValue(newValue.toString());
            preference.setSummary(colValue[i]);
            return true;
        }
        preference.setSummary((CharSequence)newValue);
        return true;
    }
}