package com.human.lw10;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Chronometer;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private GridView mGrid;
    private GridAdapter mAdapter;
    private TextView mStepScreen;
    private Chronometer mTimeScreen;
    private Integer StepCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGrid = (GridView) findViewById(R.id.field);
        mStepScreen = (TextView) findViewById(R.id.stepview);
        mTimeScreen = (Chronometer) findViewById(R.id.timeview);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String PictureCollection = settings.getString("PictureCollection", "animal");
        Integer BackgroundColor = Color.parseColor(settings.getString("BackgroundColor", "white"));

        Typeface type = Typeface.createFromAsset(getAssets(), "my-font.ttf");
        mTimeScreen.setTypeface(type);
        mStepScreen.setTypeface(type);

        StepCount = 0;
        mStepScreen.setText(StepCount.toString());

        mTimeScreen.start();

        View root = mGrid.getRootView();
        root.setBackgroundColor(BackgroundColor);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            mGrid.setNumColumns(9);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            mGrid.setNumColumns(6);
//        mGrid.setNumColumns(6);
        mGrid.setEnabled(true);

        mAdapter = new GridAdapter(this, 6, 6, PictureCollection);
        mGrid.setAdapter(mAdapter);

        mGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                mAdapter.checkOpenCells();
                if (mAdapter.openCell(position)) {
                    StepCount++;
                    mStepScreen.setText(StepCount.toString());
                }


                if (mAdapter.checkGameOver()) {
                    mTimeScreen.stop();
                    String time = mTimeScreen.getText().toString();
//                    String TextToast = "Игра закончена \nХодов: " + StepCount.toString() + "\nВремя: " + time;
//                    Toast.makeText(getApplicationContext(), TextToast, Toast.LENGTH_SHORT).show();
                    ShowGameOver();
                }
            }
        });
    }

    public void onSettingsBtnClick(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            mGrid.setNumColumns(9);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            mGrid.setNumColumns(6);
    }

    private void ShowGameOver () {

        String time = mTimeScreen.getText().toString();

        // Читаем файл с рекордами
        RecordAdapter ra = new RecordAdapter (this);
        // Добавляем новые значения
        ra.addPoint(StepCount);
        ra.addTime(time);
        // Записываем рекорды в файл
        ra.WriteRecords();


        // Диалоговое окно
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);

        // Заголовок и текст
        alertbox.setTitle("Поздравляем!");
        String TextToast = "Игра закончена \nХодов: " + StepCount.toString() + "\nВремя: " + time;
        alertbox.setMessage(TextToast);

        // Добавляем кнопку
        alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                // закрываем текущюю Activity
                finish();
            }
        });
        // показываем окно
        alertbox.show();
    }
}
