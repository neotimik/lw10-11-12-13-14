package com.human.lw10;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class StartActivity extends AppCompatActivity {
    TextView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getSupportActionBar().hide();
        logo = (TextView) findViewById(R.id.logo);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "my-font.ttf");
        logo.setTypeface(typeface);
    }

    public void onStartGameBtnClick(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void onSettingsBtnClick(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    public void onExitGameBtnClick(View view) {
        finish();
    }

    public void onRecordsBtnClick(View view) {
        startActivity(new Intent(this, Records.class));
    }
}
