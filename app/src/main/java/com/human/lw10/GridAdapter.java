package com.human.lw10;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;



public class GridAdapter extends BaseAdapter {
    private enum Status {CELL_OPEN, CELL_CLOSE, CELL_DELETE}
    private ArrayList<Status> arrStatus;
    private Context mContext;
    private Integer mCols, mRows;
    private ArrayList<String> arrPict;
    private String pictureCollection;
    private Resources mRes;

    public GridAdapter(Context mContext, Integer mCols, Integer mRows, String pictCollection) {
        this.mContext = mContext;
        this.mCols = mCols;
        this.mRows = mRows;
        arrPict = new ArrayList<String>();
        arrStatus = new ArrayList<Status>();
        pictureCollection = pictCollection;
        mRes = mContext.getResources();
        makePictArray();
        closeAllCells();
    }

    private void closeAllCells () {
        arrStatus.clear();
        for (int i = 0; i < mCols * mRows; i++)
            arrStatus.add(Status.CELL_CLOSE);
    }

    private void makePictArray() {
        arrPict.clear();
        for (int i = 0; i < ((mCols * mRows) / 2); i++) {
            arrPict.add(pictureCollection + Integer.toString(i));
            arrPict.add(pictureCollection + Integer.toString(i));
        }
        Collections.shuffle(arrPict);
    }

    @Override
    public int getCount() {
        return mCols * mRows;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view;

        if (convertView == null) {
            view = new ImageView(mContext);
        } else {
            view = (ImageView) convertView;
        }
        switch (arrStatus.get(position))
        {
            case CELL_OPEN:
                Integer drawableId = mRes.getIdentifier(arrPict.get(position), "drawable", mContext.getPackageName());
                view.setImageResource(drawableId);
                break;
            case CELL_CLOSE:
                view.setImageResource(R.drawable.close);
                break;
            default:
                view.setImageResource(R.drawable.none);
        }
        return view;
    }


    public void checkOpenCells() {
        int first = arrStatus.indexOf(Status.CELL_OPEN);
        int second = arrStatus.lastIndexOf(Status.CELL_OPEN);
        if (first == second)
            return;
        if (arrPict.get(first).equals (arrPict.get(second)))
        {
            arrStatus.set(first, Status.CELL_DELETE);
            arrStatus.set(second, Status.CELL_DELETE);
        }
        else
        {
            arrStatus.set(first, Status.CELL_CLOSE);
            arrStatus.set(second, Status.CELL_CLOSE);
        }
        return;
    }

    public boolean openCell(int position) {
        if (arrStatus.get(position) == Status.CELL_DELETE || arrStatus.get(position) == Status.CELL_OPEN)
            return false;

        if (arrStatus.get(position) != Status.CELL_DELETE)
            arrStatus.set(position, Status.CELL_OPEN);

        notifyDataSetChanged();
        return true;
    }

    public boolean checkGameOver() {
        if (arrStatus.indexOf(Status.CELL_CLOSE) < 0)
            return true;
        return false;
    }
}
